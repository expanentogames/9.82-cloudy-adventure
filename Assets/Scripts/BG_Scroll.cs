﻿using UnityEngine;

/// <summary>
/// Parallax scrolling script that should be assigned to a layer
/// </summary>
public class BG_Scroll : MonoBehaviour
{
	/// Scrolling speed
	public Vector2 speed = new Vector2(2, 2);
	/// Moving direction
	public Vector2 direction = new Vector2(-1, 0);
	/// Movement should be applied to camera
	public bool isLinkedToCamera = false;
    private float speedk;
    
	void Update()
	{
		// Movement
        speedk = GameObject.Find("PLayer").GetComponent<Rigidbody2D>().velocity.magnitude;
		Vector3 movement = new Vector3(
			speed.x * direction.x,
			speed.y * direction.y*speedk/100,
			0);
		
		movement *= Time.deltaTime;
   
        transform.Translate(movement);
		
		// Move the camera
		if (isLinkedToCamera)
		{
			Camera.main.transform.Translate(movement);
		}
	}
}