﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{	
	public float health = 100f;					// Здоровье ГГ
	public float repeatDamagePeriod = 2f;		// Как часто игрок может получать урон
	public AudioClip[] ouchClips;				// Массив, звуко в проигрываемых при нанесении урона
	public float hurtForce = 10f;				// Сила с которой ГГ отталкивается от предмета, нанесшего урон
	public float damageAmount = 10f;			// Размер урона, наносимого объектами
    public bool smack;

	private SpriteRenderer healthBar;			// Спрайт полосы здоровья
	private float lastHitTime;					// The time at which the player was last hit.
	private Vector3 healthScale;				// масштаб полосы здоровья (с  полным хп)
	private PlayerControl playerControl;		// скрипт player контроллера
	private Animator anim;						// значение аниматора плеера


	void Awake ()
	{
		// настройка значений
		playerControl = GetComponentInParent<PlayerControl>();
		healthBar = GameObject.Find("blood").GetComponent<SpriteRenderer>();
		anim = GetComponentInParent<Animator>();

		// Getting the intial scale of the healthbar (whilst the player has full health).
		healthScale = healthBar.transform.localScale;
	}


	void OnCollisionEnter2D (Collision2D col)
	{
		// If the colliding gameobject is an Enemy...
		if(col.gameObject.tag == "ironCloud")
		{
			// ... and if the time exceeds the time of the last hit plus the time between hits...
			if (Time.time > lastHitTime + repeatDamagePeriod) 
			{
				// ... and if the player still has health...
				if(health > 0f)
				{
					// ... take damage and reset the lastHitTime.
					TakeDamage(col.transform); 
					lastHitTime = Time.time; 
				}
				// If the player doesn't have health, do some stuff, let him fall into the river to reload the level.
			}
		}
	}
    void OnCollisionExit2D(Collision2D col)
    { smack = false; }


	void TakeDamage (Transform enemy)
	{
		// Make sure the player can't jump.

		// Create a vector that's from the enemy to the player with an upwards boost.
		Vector3 hurtVector = transform.position - enemy.position + Vector3.up * 5f;

		// Add a force to the player in the direction of the vector and multiply by the hurtForce.
		rigidbody2D.AddForce(hurtVector * hurtForce);

		// Reduce the player's health by 10.
		health -= damageAmount;

		// Update what the health bar looks like.
		UpdateHealthBar();

		// Play a random clip of the player getting hurt.
		int i = Random.Range (0, ouchClips.Length);
		AudioSource.PlayClipAtPoint(ouchClips[i], transform.position);
	}


	public void UpdateHealthBar ()
	{
		// Set the health bar's colour to proportion of the way between green and red based on the player's health.
		healthBar.material.color = Color.Lerp(Color.green, Color.red, 1 - health * 0.01f);

		// Set the scale of the health bar to be proportional to the player's health.
		healthBar.transform.localScale = new Vector3(healthScale.x * health * 0.01f, 1, 1);
	}
}
