﻿using UnityEngine;
using System.Collections;

public class Player_Collider_Function : MonoBehaviour
{
		public float HP = 100;
		protected Animator animator;
		private Rigidbody2D rg;
        public float Procent;
		// Use this for initialization
		void Start ()
		{
				animator = GetComponentInParent<Animator> ();
				rg = GetComponentInParent<Rigidbody2D> ();
                Procent = 1 / HP;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void OnCollisionEnter2D (Collision2D coll)
		{
				if (HP > 0)	
				HP -=5;
                if (coll.gameObject.tag == "ironCloud")
						animator.SetBool ("hit", true);
		
		}

		void OnCollisionExit2D (Collision2D coll)
		{
				if (coll.gameObject.tag == "ironCloud")
						animator.SetBool ("hit", false);
		
		}
}
