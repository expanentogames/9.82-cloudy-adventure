﻿using UnityEngine;
using System.Collections;

public class StaminaCapacity : MonoBehaviour {

    public float Stamina=200f;
    private float StaminaMax=0f;
    public float Procent;

	// Use this for initialization
	void Start () {
        StaminaMax = Stamina;
        Procent = 1 / Stamina;
	}
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0)) && Stamina > 0)
            Stamina--;
        else if (Stamina < StaminaMax)
            Stamina++;
	}
}
