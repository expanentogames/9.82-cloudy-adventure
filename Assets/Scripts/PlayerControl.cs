using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
		public float maxSpeed = 10f;//Указываю максимальную скорость
		public Animator animator;
		public bool KeyboardInput;
		public float prepos;
		private float dir;
		public float Rspeed = 15f;

		// Use this for initialization
		void Start ()
		{
				animator = GetComponent<Animator> ();
		}

		void OnBecameInvisible ()
		{
				prepos = -gameObject.transform.position.x;
				gameObject.transform.position = new Vector3 (prepos, gameObject.transform.position.y, 0f);


		}



		// Update is called once per frame
		void Update ()
		{
	

				if ((Input.GetKey (KeyCode.Space) || Input.GetMouseButton (0)) && GetComponent<StaminaCapacity>().Stamina>5) {//если нажат пробел или мышь
                    
			if(rigidbody2D.velocity.magnitude > maxSpeed/2)
			{rigidbody2D.AddForce (new  Vector2 (0, 10));}// добавляем силу 200Н вверх к персонажу, не самы йдачный вариант
						animator.SetBool ("Plane", true);
				} else {
						if (animator.GetBool ("Plane"))
								animator.SetBool ("Plane", false);
				}



				if (KeyboardInput) {
						if (Input.GetKey (KeyCode.LeftArrow)) {//событие полета влево
								rigidbody2D.AddForce (new  Vector2 (-Rspeed, 0));
								animator.SetBool ("Left", true);
						} else {
								if (animator.GetBool ("Left"))
										animator.SetBool ("Left", false);
						}

						if (Input.GetKey (KeyCode.RightArrow)) {//событие полета вправо
								rigidbody2D.AddForce (new  Vector2 (Rspeed, 0));
								animator.SetBool ("right", true);
						} else {
								if (animator.GetBool ("right"))
										animator.SetBool ("right", false);

						}
                                
				}
		}

		void FixedUpdate ()
		{
				if (rigidbody2D.velocity.magnitude > maxSpeed) {
						rigidbody2D.velocity = rigidbody2D.velocity.normalized * maxSpeed;
				}
				dir = Input.acceleration.x;

				if (Mathf.Abs (dir) > 0.20f) 
						rigidbody2D.AddForce (new Vector2 (dir * Rspeed, 0));
				else {
						if (animator.GetBool ("right") || animator.GetBool ("Left")) {
								animator.SetBool ("right", false);
								animator.SetBool ("Left", false);
						}
				}
				if (dir > 0.20)
						animator.SetBool ("right", true);
				if (dir < -0.20)
						animator.SetBool ("Left", true);
		}

}
