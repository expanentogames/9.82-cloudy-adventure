﻿using UnityEngine;
using System.Collections;
/*такое ощущение что этот скрипт больше для стрелки чем для облака... ну уж нет, переделывать это я не буду*/
public class Cloud_Script : MonoBehaviour {

    public GameObject arrow;
    public Transform icon;
    public Transform camera;


	// Use this for initialization
	void Start () {
        if (!arrow == false)   //такую вот шняжку я извлек для себя на ОАиПе, может крава иногда и дело говорит?...
        {
            Instantiate(arrow, new Vector3(transform.position.x,transform.position.y,5), new Quaternion(0,0,-90,90));//создаю стрелка как только спавнится облако(void Start)
            arrow = GameObject.Find("arrow(Clone)");//вот... и начинаю следить за новосозданной стрелкой вместо префаба
            camera = GameObject.Find("uicamera").transform;//камера, относительно которой размещается стрелка
            arrow.name = ("takenarrow");//переименовываю объект, который отслеживаю (чтобы последующие облака не путались, за какой из стрелок ему  следить)

        
            icon= arrow.transform.FindChild("icon");//обращаюсь к пока еще болванке иеонки на стрелке

            icon.GetComponent<Transform>().localScale = new Vector3(0.3f, 0.3f, 1f);//задаю ему размер
            icon.Rotate(new Vector3(0,0,90));//и поворачиваю

            icon.GetComponent<SpriteRenderer>().sprite = gameObject.GetComponent<SpriteRenderer>().sprite;//а теперь... я получаю спрайт с облака, на котором висит этот скрипт и вешаю его на значок на стрелке
        }
	}
	
	// Update is called once per frame
	void Update () {
        float disx;
        float disy;
        float arrr;


        if (!arrow == false)//для чего это нужно? этот скрипт весит на ВСЕХ облаках(металлических и нет) но только металлические згружают стрелку(для остальных я просто не указывал, какую) оно и так бы работало но засирало консоль ошибками
        {   arrow.transform.position = new Vector3(arrow.transform.position.x, camera.position.y-8,5);//это.. по моему.. да, я слежу за тем чтобы стрелка всегда была на одном уровне относительно камеры (и двигалась относительно мира)

        disx = Mathf.Abs(arrow.transform.position.x-transform.position.x);
        disy = arrow.transform.position.y - transform.position.y;

        arrr = Mathf.Tan(disx/disy);

        if (transform.position.y > GameObject.Find("Main Camera").transform.position.y)
        {
            Destroy(arrow);
            Destroy(gameObject);
        }
    }
	}

	void OnBecameInvisible ()//когда объект становится невидимиым
	{
		Destroy (gameObject);//уничтожаю облако, когда оно скрывается за экраном (очищаю память)
	}
    void OnBecameVisible()
    {//когда объект становится видимым(удаляется стрелка)
        if (!arrow == false)
        Destroy(arrow);//и уничтожаю ее как только облако появляется
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        print("TRIGGER");
        if (!arrow == false)
            Destroy(arrow);
        Destroy(gameObject);
    }

}
