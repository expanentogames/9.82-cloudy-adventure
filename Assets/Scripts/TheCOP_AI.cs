﻿using UnityEngine;
using System.Collections;

public class TheCOP_AI : MonoBehaviour {

    public float maxSpeed = 7f;//Указываю максимальную скорость
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate() {
        if (rigidbody2D.velocity.magnitude > maxSpeed)
        {
            rigidbody2D.velocity = rigidbody2D.velocity.normalized * maxSpeed;
        }
    
    }
}
